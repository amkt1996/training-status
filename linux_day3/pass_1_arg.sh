#! /bin/bash

print_table(){
    echo "Table of $1"
    num=$1
    for i in {1..10}
    do
        echo "$num*$i" | bc
    done
}
print(){
    echo "Five Times"
    num=5
    while [ $num -gt 0 ]
    do
        echo "INIDA"
        (( num-- ))
    done
}

if [ $1 -gt 10 ]
then
    print $1
else
    print_table $1
fi