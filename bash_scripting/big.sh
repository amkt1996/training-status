#! /bin/bash
printf "\tEnter a list of numbers, with spaces: "
read -a ARRAY
BIG=${ARRAY[1]}
for i in ${ARRAY[@]}
do
    if [ $i -gt $BIG ]
    then
        BIG=$i
    else
        continue
    fi
done

echo "Largest number is $BIG"