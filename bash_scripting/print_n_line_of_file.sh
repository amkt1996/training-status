#! /bin/bash

#print n lines of file

echo -e "Name of file: \c"
read file_name

echo -e "Number of lines: \c"
read num
for i in *
do
    if [ $i == $file_name ]
    then
        while read p
        do
            if [ $num -gt 0 ]
            then
                echo $p
                ((num--))
            fi
        done < $file_name
    fi
done
