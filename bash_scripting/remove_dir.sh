#! /bin/bash

#take the details
echo -e "Enter the dir to remove: \c"
read dir_name

echo -e "Are you sure want to remove (type y:n): \c"
read ans

if [ $ans == y ]
then
    rm -rf $dir_name
else
    echo "try again"
fi