#! /bin/bash
# declare a function for search a file
function search(){
    dir=$1
    pattern=$2
    cd $dir
    ls $pattern
}

#input the details
echo -e "Enter the file path :\c"
read dir
echo -e "Enter the pattern with start: \c"
read pattern


search $dir $pattern